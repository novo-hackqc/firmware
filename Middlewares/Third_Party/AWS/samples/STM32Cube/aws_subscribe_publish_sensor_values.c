/**
  ******************************************************************************
  * @file    subscribe_publish_sensor_values.c
  * @author  MCD Application Team
  * @brief   Control of the measurement sampling and MQTT reporting loop.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#ifdef CLD_OTA
#include "rfu.h"
#endif
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_jobs_interface.h"
#include "iot_flash_config.h"
#ifdef SENSOR
#include "sensors_data.h"
#endif
#include "msg.h"

/* Private defines ------------------------------------------------------------*/
#define MQTT_CONNECT_MAX_ATTEMPT_COUNT  3
#define TELEMETRY_INTERVAL              10    /*< When the telemetry is active, the data are published every TELEMETRY_INTERVAL seconds. */
#define TELEMETRY_LIFETIME              300   /*< The telemetry publication stops after TELEMETRY_LIFETIME seconds. */

//#define USE_JOBS

#define aws_json_pre        "{\"state\":{\"reported\":"
#define aws_json_desired    "{\"state\":{\"desired\":"
#define aws_json_post       "}}"

/* Private types ---------------------------------------------------------*/
typedef enum
{
	COMMAND_REBOOT = 1,
	COMMAND_SETLED,
	COMMAND_REFRESHPOSITION,

	COMMAND_MAXID
}BORNE_COMMAND_ID_t;


/* Private variables ---------------------------------------------------------*/
static const char *pDeviceName;
static bool ledstateOn = false;
static char cPubTopic[MAX_SHADOW_TOPIC_LENGTH_BYTES] = ""; /* Publish Topic */
static char cSubTopic[MAX_SHADOW_TOPIC_LENGTH_BYTES] = ""; /* Subscribe Topic */

float fwaterLevel = 0;
float fLatitude = 0;
float fLongitude = 0;

#ifndef USE_JOBS
static jsmn_parser jsonParser;
static jsmntok_t jsonTokenStruct[MAX_JSON_TOKEN_EXPECTED];
static int32_t tokenCount;
#endif

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
bool g_continue = true;     /*< Controls the MQTT client loop. */
#ifdef CLD_OTA
static bool g_ExecuteFOTA;
#define FOTA_URI_LEN 300
char g_firmware_update_uri[FOTA_URI_LEN+1];
iot_state_t write_ota_state;
#define FOTA_INSTALLATION_NOT_REQUESTED ((uint8_t)0)
#define FOTA_INSTALLATION_REQUESTED ((uint8_t)1)
#endif

/* Private function prototypes -----------------------------------------------*/
static void MQTTcallbackHandler(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen, IoT_Publish_Message_Params *params, void *pData);
int32_t comp_left_ms(uint32_t init, uint32_t now, uint32_t timeout);

/* Functions Definition ------------------------------------------------------*/
int cloud_device_enter_credentials(void)
{
  int ret = 0;
  iot_config_t iot_config;

  memset(&iot_config, 0, sizeof(iot_config_t));

  printf("\nEnter server address: (example: xxx.iot.region.amazonaws.com) \n");
  getInputString(iot_config.server_name, USER_CONF_SERVER_NAME_LENGTH);
  msg_info("read: --->\n%s\n<---\n", iot_config.server_name);

  printf("\nEnter device name: (example: mything1) \n");
  getInputString(iot_config.device_name, USER_CONF_DEVICE_NAME_LENGTH);
  msg_info("read: --->\n%s\n<---\n", iot_config.device_name);

  if (setIoTDeviceConfig(&iot_config) != 0)
  {
    ret = -1;
    msg_error("Failed programming the IoT device configuration to Flash.\n");
  }

  return ret;
}


bool app_needs_root_ca(void)
{
  return true;
}


bool app_needs_device_keypair(void)
{
  return true;
}


bool app_needs_iot_config(void)
{
  return true;
}


/**
* @brief MQTT disconnect callback hander
*
* @param pClient: pointer to the AWS client structure
* @param data:
* @return no return
*/
static void disconnectCallbackHandler(AWS_IoT_Client *pClient, void *data)
{
  msg_warning("MQTT Disconnect\n");
  IoT_Error_t rc = FAILURE;

  if (NULL == data)
  {
    return;
  }

  AWS_IoT_Client *client = (AWS_IoT_Client *)data;

  if (aws_iot_is_autoreconnect_enabled(client))
  {
    msg_info("Auto Reconnect is enabled, Reconnecting attempt will start now\n");
  }
  else
  {
    msg_warning("Auto Reconnect not enabled. Starting manual reconnect...\n");
    rc = aws_iot_mqtt_attempt_reconnect(client);

    if (NETWORK_RECONNECTED == rc)
    {
      msg_warning("Manual Reconnect Successful\n");
    }
    else
    {
      msg_warning("Manual Reconnect Failed - %d\n", rc);
    }
  }
}

/* Exported functions --------------------------------------------------------*/

/**
* @brief MQTT subscriber callback hander
*
* called when data is received from AWS IoT Thing (message broker)
* @param MQTTCallbackParams type parameter
* @return no return
*/
static void MQTTcallbackHandler(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen, IoT_Publish_Message_Params *params, void *pData)
{
  char ledState[6];
  uint32_t u32bootDelay;
  uint32_t u32bootPartition;
  uint32_t u32command = 0;
  int i = 0;

  printf("%s\n\r", (char*)params->payload);

  jsmn_init(&jsonParser);
  tokenCount = jsmn_parse(&jsonParser, params->payload, (int) params->payloadLen, jsonTokenStruct, MAX_JSON_TOKEN_EXPECTED);

  if (tokenCount < 0)
  {
    IOT_WARN("Failed to parse JSON: %d", tokenCount);
    return;
  }

  /* Assume the top-level element is an object */
  if (tokenCount < 1 || jsonTokenStruct[0].type != JSMN_OBJECT)
  {
    IOT_WARN("Top Level is not an object");
    return;
  }

  i = 0;
  while((i < tokenCount) && (jsoneq(params->payload, &jsonTokenStruct[i], "deviceId") != 0))
  {
    i++;
  }
  if(jsoneq(params->payload, &jsonTokenStruct[i+1], "3077b700-5bdb-11ea-bc55-0242ac130003") != 0)
  {
  	msg_error("Wrong deviceId.\n");
  	return;
  }

  while((i < tokenCount) && (jsoneq(params->payload, &jsonTokenStruct[i], "commandId") != 0))
  {
    i++;
  }
  parseUnsignedInteger32Value(&u32command, params->payload, &jsonTokenStruct[i+1]);

  switch(u32command)
  {
    case(COMMAND_REBOOT):
    {
    	i = 0;
    	while((i < tokenCount) && (jsoneq(params->payload, &jsonTokenStruct[i], "arguments") != 0))
    	{
    		i++;
    	}
    	if(jsonTokenStruct[i+1].type == JSMN_ARRAY)
    	{
    		parseUnsignedInteger32Value(&u32bootDelay, params->payload, &jsonTokenStruct[i+2]);
    	    parseUnsignedInteger32Value(&u32bootPartition, params->payload, &jsonTokenStruct[i+3]);
    	}
    	else
    	{
    	  msg_error("Arguments or not in an array.\n");
    	  return;
    	}
    }
    break;

    case(COMMAND_SETLED):
	{
    	i = 0;
    	while((i < tokenCount) && (jsoneq(params->payload, &jsonTokenStruct[i], "arguments") != 0))
    	{
    	  i++;
    	}
    	if(jsonTokenStruct[i+1].type == JSMN_ARRAY)
    	{
    	  parseStringValue(ledState, 6, params->payload, &jsonTokenStruct[i+2]);
    	  // set led state;
    	}
    	else
    	{
    	  msg_error("Arguments or not in an array.\n");
    	  return;
    	}
	}
    break;

    case(COMMAND_REFRESHPOSITION):
	{
      // refresh position
	}
    break;

    default:
    {
    	 msg_error("Invalid CommandId %d\n", (int)u32command);
    }
    break;
  }
}



/**
* @brief main entry function to AWS IoT code
*
* @param no parameter
* @return AWS_SUCCESS: 0
          FAILURE: -1
*/
void cloud_run(void const *arg)
{
  const char *serverAddress = NULL;
  const char *pCaCert;
  const char *pClientCert;
  const char *pClientPrivateKey;
  char cPayload[1000];
  char const *deviceName;
  const char deviceId[] =  "3077b700-5bdb-11ea-bc55-0242ac130003";
  int connectCounter;
  IoT_Error_t rc = FAILURE;
#ifdef CLD_OTA
  int ret = 0;
#endif

  AWS_IoT_Client client;
  memset(&client, 0, sizeof(AWS_IoT_Client));
  IoT_Client_Init_Params mqttInitParams = iotClientInitParamsDefault;
  IoT_Client_Connect_Params connectParams = iotClientConnectParamsDefault;

#ifdef CLD_OTA
  g_ExecuteFOTA = false;
#endif
  getIoTDeviceConfig(&deviceName);
  if (strlen(deviceName) >= MAX_SIZE_OF_THING_NAME)
  {
    msg_error("The length of the device name stored in the iot user configuration is larger than the AWS client MAX_SIZE_OF_THING_NAME.\n");
    return;
  }

  snprintf(cPubTopic, sizeof(cPubTopic), "devices/%s/waterLevel", deviceId);
  snprintf(cSubTopic, sizeof(cSubTopic), "devices/%s/commands", deviceId);

  msg_info("AWS IoT SDK Version %d.%d.%d-%s\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

  getServerAddress(&serverAddress);
  getTLSKeys(&pCaCert, &pClientCert, &pClientPrivateKey);
  mqttInitParams.enableAutoReconnect = false; /* We enable this later below */
  mqttInitParams.pHostURL = (char *) serverAddress;
  mqttInitParams.port = AWS_IOT_MQTT_PORT;
  mqttInitParams.pRootCALocation = (char *) pCaCert;
  mqttInitParams.pDeviceCertLocation = (char *) pClientCert;
  mqttInitParams.pDevicePrivateKeyLocation = (char *) pClientPrivateKey;
  mqttInitParams.mqttCommandTimeout_ms = 20000;
  mqttInitParams.tlsHandshakeTimeout_ms = 5000;
  mqttInitParams.isSSLHostnameVerify = true;
  mqttInitParams.disconnectHandler = disconnectCallbackHandler;
  mqttInitParams.disconnectHandlerData = NULL;

  rc = aws_iot_mqtt_init(&client, &mqttInitParams);

  if (AWS_SUCCESS != rc)
  {
    msg_error("aws_iot_mqtt_init returned error : %d\n", rc);
    return;
  }

  getIoTDeviceConfig(&pDeviceName);
  connectParams.keepAliveIntervalInSec = 30;
  connectParams.isCleanSession = true;
  connectParams.MQTTVersion = MQTT_3_1_1;
  connectParams.pClientID = (char *) pDeviceName;
  connectParams.clientIDLen = (uint16_t) strlen(pDeviceName);
  connectParams.isWillMsgPresent = false;


  connectCounter = 0;

  do
  {
    connectCounter++;
    printf("MQTT connection in progress:   Attempt %d/%d ...\n", connectCounter, MQTT_CONNECT_MAX_ATTEMPT_COUNT);
    rc = aws_iot_mqtt_connect(&client, &connectParams);
  }
  while ((rc != AWS_SUCCESS) && (connectCounter < MQTT_CONNECT_MAX_ATTEMPT_COUNT));

  if (AWS_SUCCESS != rc)
  {
    msg_error("\nError(%d) connecting to %s:%d\n", rc, mqttInitParams.pHostURL, mqttInitParams.port);
    return;
  }
  else
  {
    printf("\nConnected to %s:%d\n", mqttInitParams.pHostURL, mqttInitParams.port);
  }

  /*
  * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
  *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
  *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
  */
  rc = aws_iot_mqtt_autoreconnect_set_status(&client, true);

  if (AWS_SUCCESS != rc)
  {
    msg_error("Unable to set Auto Reconnect to true - %d\n", rc);

    if (aws_iot_mqtt_is_client_connected(&client))
    {
      aws_iot_mqtt_disconnect(&client);
    }

    return;
  }

  /* Subscribe to the "shadow accepted" topic */
  rc = aws_iot_mqtt_subscribe(&client, cSubTopic, strlen(cSubTopic), QOS0, MQTTcallbackHandler, NULL);

  if (AWS_SUCCESS != rc)
  {
    msg_error("Error subscribing : %d\n", rc);
    return;
  }
  else
  {
    msg_info("Subscribed to topic %s\n", cSubTopic);
  }

  IoT_Publish_Message_Params paramsQOS1 =
  {
    .qos = QOS1,
    .isRetained = 0,
    .isDup = 0,
    .id = 0,
    .payload = NULL,
    .payloadLen = 0
  };

  printf("Start publishing to the %s topic\n", cPubTopic);

  while ((NETWORK_ATTEMPTING_RECONNECT == rc || NETWORK_RECONNECTED == rc || AWS_SUCCESS == rc)  && g_continue)
  {
    /* Max time the yield function will wait for read messages */
    rc = aws_iot_mqtt_yield(&client, 10);

    if (NETWORK_ATTEMPTING_RECONNECT == rc)
    {
      /* Delay to let the client reconnect */
      HAL_Delay(1000);
      msg_info("Attempting to reconnect\n");
      /* If the client is attempting to reconnect we will skip the rest of the loop */
      continue;
    }
    if (NETWORK_RECONNECTED == rc)
    {
      msg_info("Reconnected.\n");
    }


     printf("Sending water level  AWS.\n");
     ledstateOn = !ledstateOn;


     (void) snprintf(cPayload, AWS_IOT_MQTT_TX_BUF_LEN, "{\"deviceId\":\"%s\",\"waterLevel\":%f,\"lat\":%f,\"lng\":%f}" ,deviceId, fwaterLevel, fLatitude, fLongitude);

     paramsQOS1.payload = cPayload;
     paramsQOS1.payloadLen = strlen(cPayload) + 1;

     do
     {
       rc = aws_iot_mqtt_publish(&client, cPubTopic, strlen(cPubTopic), &paramsQOS1);

       if (rc == AWS_SUCCESS)
       {
         printf("\nPublished to topic %s:", cPubTopic);
         printf("%s\n", cPayload);
       }
     } while (MQTT_REQUEST_TIMEOUT_ERROR == rc);

     HAL_Delay(15000);
  } /* End of while */

  msg_debug("Exit from subscribe_publish_sensor_values() main loop.\n");
  /* Wait for all the messages to be received */
  aws_iot_mqtt_yield(&client, 10);

  rc = aws_iot_mqtt_disconnect(&client);
  /* Free up payload table */

#ifdef CLD_OTA
  if (g_ExecuteFOTA)
  {
    IOT_INFO("Updating Firmware with URL: %s\n", g_firmware_update_uri);
    ret = rfu_update(g_firmware_update_uri, lUserConfigPtr->tls_root_ca_cert);

    if (ret == RFU_OK)
    {
      IOT_INFO("  -- Image downloaded.\n");

      /*
       * Memorize that the bootloader will run the Firmware Installation procedure.
       * This will be useful at next boot to determine the proper status ('Error' or 'Current') to be reported.
       */
      memset(&write_ota_state, 0x00, sizeof(iot_state_t));

      write_ota_state.fota_state = FOTA_INSTALLATION_REQUESTED;
      /*
       * Store the current firmware version in FLASH.
       * This is used at next boot to determine if the installation procedure succeeded or not.
       * If the running firmware has the same version this means the installation procedure failed.
       * If the running firmware has a different version then it means the installation procedure succeeded.
       */
      snprintf(write_ota_state.prev_fw_version, IOT_STATE_FW_VERSION_LEN, "%d.%d.%d", FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_VERSION_PATCH);

      ret = setIoTState(&write_ota_state);

      if (0 != ret)
      {
        msg_error("setIoTDeviceConfig(FOTA_INSTALLATION_REQUESTED) failed.\n");
      }
      else
      {
        msg_info("%s memorized as previous FW version.\n", write_ota_state.prev_fw_version);
        /* Reboot after everything is successfuly stored */
        printf("     Reboot\n");
        HAL_Delay(1000U);
        NVIC_SystemReset();
      }
    }
    else
    {
      msg_info("Problem in RFU update (%d)\n", ret);
    }

  }
#endif
}


/**
 * @brief   Return the integer difference between 'init + timeout' and 'now'.
 *          The implementation is robust to uint32_t overflows.
 * @param   In:   init      Reference index.
 * @param   In:   now       Current index.
 * @param   In:   timeout   Target index.
 * @retval  Number of units from now to target.
 */
int32_t comp_left_ms(uint32_t init, uint32_t now, uint32_t timeout)
{
  uint32_t elapsed = 0;

  if (now < init)
  { /* Timer wrap-around detected */
    /* printf("Timer: wrap-around detected from %d to %d\n", init, now); */
    elapsed = UINT32_MAX - init + now;
  }
  else
  {
    elapsed = now - init;
  }

  return timeout - elapsed;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

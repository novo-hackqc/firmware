/*
 * APP_sensor_TOF.c
 *
 *  Created on: Mar. 1, 2020
 *      Author: f.helie
 */


#include "APP_sensor_TOF.h"

#define VL53_ADDR       	0x52
#define VL53_OFFSET        	76U
#define VL53_SIG_THRESHOLD 	100U

void TOF_init()
{
	int Vl53Status = 0;
	uint8_t byteData = 0;
	uint8_t sensorState = 0;
	uint8_t dataReady = 0;
	uint16_t u16Distance = 0;
	uint16_t u16WordData = 0;
	int16_t offset = 0;

	Vl53Status = VL53L1_RdByte(VL53_ADDR, 0x010F, &byteData);
	Vl53Status = VL53L1_RdByte(VL53_ADDR, 0x0110, &byteData);
	Vl53Status = VL53L1_RdWord(VL53_ADDR, 0x010F, &u16WordData);

	while(sensorState == 0)
	{
	  Vl53Status = VL53L1X_BootState(VL53_ADDR, &sensorState);
	}

	Vl53Status = VL53L1X_SensorInit(VL53_ADDR);
	Vl53Status = VL53L1X_SetDistanceMode(VL53_ADDR, 2); /*1 = short, 2 = long*/
	Vl53Status = VL53L1X_SetTimingBudgetInMs(VL53_ADDR, 500);
	Vl53Status = VL53L1X_SetInterMeasurementInMs(VL53_ADDR, 500);
	Vl53Status = VL53L1X_SetROI(VL53_ADDR, 4, 4);

	Vl53Status = VL53L1X_SetOffset(VL53_ADDR, VL53_OFFSET);
	Vl53Status = VL53L1X_SetSignalThreshold(VL53_ADDR, VL53_SIG_THRESHOLD);

	Vl53Status = VL53L1X_StartRanging(VL53_ADDR);

	while (1)
	  {
	    /* USER CODE END WHILE */

	    /* USER CODE BEGIN 3 */
		  while(dataReady == 0)
		  {
			  Vl53Status = VL53L1X_CheckForDataReady(VL53_ADDR, &dataReady);
			  HAL_Delay(2);
		  }
		  dataReady = 0;
		  Vl53Status = VL53L1X_GetDistance(VL53_ADDR, &u16Distance);
		  //printf("%d\n\r", u16Distance);
	  }
}

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
EXECUTABLES := 
OBJS := 
C_DEPS := 
OBJCOPY_BIN := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Application/AWS \
Application/SW4STM32 \
Application/Time \
Application/User \
Application/Utils \
Drivers/BSP/BG96_STMOD+/at_modem_bg96 \
Drivers/BSP/STM32L496G-Discovery \
Drivers/BSP/UG96_STMOD+/at_modem_ug96 \
Drivers/CMSIS \
Drivers/STM32L4xx_HAL_Driver \
Middlewares/ST/STM32_Cellular/Core/Cellular_Api/Cellular_init \
Middlewares/ST/STM32_Cellular/Core/Cellular_Api/Com \
Middlewares/ST/STM32_Cellular/Core/Cellular_Api/Data_Cache \
Middlewares/ST/STM32_Cellular/Core/Cellular_Api/Nifman \
Middlewares/ST/STM32_Cellular/Core/Cellular_Api/Radio_Mngt \
Middlewares/ST/STM32_Cellular/Core/Cellular_Service/AT_Service \
Middlewares/ST/STM32_Cellular/Core/Cellular_Service/Radio_Service/Cellular \
Middlewares/ST/STM32_Cellular/Interface/IPC \
Middlewares/ST/STM32_Cellular/Porting/PPPosif \
Middlewares/ST/STM32_Cellular/Utilities/Misc/Cmd \
Middlewares/ST/STM32_Cellular/Utilities/Misc/Error_Handler \
Middlewares/ST/STM32_Cellular/Utilities/Misc/Stack_Analysis \
Middlewares/ST/STM32_Cellular/Utilities/Misc/Trace_Interface \
Middlewares/ST/STM32_Connect_Library/core \
Middlewares/ST/STM32_Connect_Library/netif \
Middlewares/ST/STM32_Connect_Library/services \
Middlewares/Third_Party/AWS \
Middlewares/Third_Party/FreeRTOS/CMSIS_RTOS \
Middlewares/Third_Party/FreeRTOS \
Middlewares/Third_Party/FreeRTOS/portable \
Middlewares/Third_Party/MbedTLS \
Middlewares/Third_Party/jsmn \


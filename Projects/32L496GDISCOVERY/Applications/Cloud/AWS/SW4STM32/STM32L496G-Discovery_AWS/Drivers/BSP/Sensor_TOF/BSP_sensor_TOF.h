/*
 * Sensor_TOF.h
 *
 *  Created on: Mar. 1, 2020
 *      Author: f.helie
 */

#ifndef DRIVERS_BSP_SENSOR_TOF_BSP_SENSOR_TOF_H_
#define DRIVERS_BSP_SENSOR_TOF_BSP_SENSOR_TOF_H_

#include "stm32l4xx_hal.h"

extern I2C_HandleTypeDef TOF_hi2c1;
void BSP_TOF_Init(void);

#endif /* DRIVERS_BSP_SENSOR_TOF_BSP_SENSOR_TOF_H_ */

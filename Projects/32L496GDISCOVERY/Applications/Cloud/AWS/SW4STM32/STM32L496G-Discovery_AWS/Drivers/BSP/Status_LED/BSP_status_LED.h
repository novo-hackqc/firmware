/*
 * BSP_status_LED.h
 *
 *  Created on: Mar. 1, 2020
 *      Author: f.helie
 */

#ifndef DRIVERS_BSP_STATUS_LED_BSP_STATUS_LED_H_
#define DRIVERS_BSP_STATUS_LED_BSP_STATUS_LED_H_

#include "main.h"

#define RED_LED_PIN		GPIO_PIN_6
#define RED_LED_PORT	GPIOI
#define GREEN_LED_PIN	GPIO_PIN_9
#define GREEN_LED_PORT	GPIOB
#define BLUE_LED_PIN	GPIO_PIN_15
#define BLUE_LED_PORT	GPIOH


typedef enum
{
    RED,
	YELLOW,
    GREEN,
	TURQUOISE,
    BLUE,
	PURPLE,
	WHITE,
	OFF,
} LedColor_t;



#endif /* DRIVERS_BSP_STATUS_LED_BSP_STATUS_LED_H_ */

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
EXECUTABLES := 
OBJS := 
C_DEPS := 
OBJCOPY_BIN := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Application/AWS \
Application/SW4STM32 \
Application/Time \
Application/User \
Application/Utils \
Drivers/BSP/B-L475E-IOT01 \
Drivers/BSP/Components/HTS221 \
Drivers/BSP/Components/LIS3MDL \
Drivers/BSP/Components/LPS22HB \
Drivers/BSP/Components/LSM6DSL \
Drivers/BSP/Components/es_wifi \
Drivers/BSP/Components/vl53l0x \
Drivers/CMSIS \
Drivers/STM32L4xx_HAL_Driver \
Middlewares/ST/STM32_Connect_Library/core \
Middlewares/ST/STM32_Connect_Library/netif \
Middlewares/ST/STM32_Connect_Library/services \
Middlewares/Third_Party/AWS \
Middlewares/Third_Party/MbedTLS \
Middlewares/Third_Party/jsmn \

